# Copyright 2013 Bing Sun
# Copyright 2009, 2010, 2014 Ingmar Vanhassel
# Distributed under the terms of the GNU General Public License v2

require bash-completion zsh-completion elisp [ with_opt=true source_directory=emacs ]

export_exlib_phases src_prepare src_compile src_test src_install

SUMMARY="Thread-based email index, search and tagging"
DESCRIPTION="
'Not much mail' is what Notmuch thinks about your email collection. Even if you
receive 12000 messages per month or have on the order of millions of messages
that you've been saving for decades. Regardless, Notmuch will be able to
quickly search all of it. It's just plain not much mail.

'Not much mail' is also what you should have in your inbox at any time. Notmuch
gives you what you need, (tags and fast search), so that you can keep your
inbox tamed and focus on what really matters in your life, (which is surely not
email).

Notmuch is an answer to Sup. Sup is a very good email program written by
William Morgan (and others) and is the direct inspiration for Notmuch. Notmuch
began as an effort to rewrite performance-critical pieces of Sup in C rather
than ruby. From there, it grew into a separate project. One significant
contribution Notmuch makes compared to Sup is the separation of the
indexer/searcher from the user interface. (Notmuch provides a library interface
so that its indexing/searching/tagging features can be integrated into any
email program.)

Notmuch is not much of an email program. It doesn't receive messages (no POP or
IMAP suport). It doesn't send messages (no mail composer, no network code at
all). And for what it does do (email search) that work is provided by an
external library, Xapian. So if Notmuch provides no user interface and Xapian
does all the heavy lifting, then what's left here? Not much.
"
HOMEPAGE="http://notmuchmail.org/"
DOWNLOADS="${HOMEPAGE}/releases/${PNV}.tar.gz"

LICENCES="GPL-3"
SLOT="0"
MYOPTIONS="emacs python ruby
    vim-plugin [[ description = [ Install vim plugin ] requires = ruby ]]
"

DEPENDENCIES="
    build:
        dev-util/pkg-config
    build+run:
        dev-db/xapian-core
        dev-libs/talloc
        net-utils/gmime:=[>=2.6&<2.7]
        python? ( dev-lang/python:= )
        ruby? ( dev-lang/ruby:= )
    run:
        vim-plugin? ( app-editors/vim[ruby] )
    suggestion:
        app-crypt/gnupg [[ description = [ Support for PGP/MIME ] ]]
    test:
        app-crypt/gnupg
        app-editors/emacs
        app-misc/dtach
"

BUGS_TO="ingmar@exherbo.org"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --hates={bindir,datarootdir,sbindir,docdir,disable-silent-rules,enable-fast-install}
    --without-{emacs,bash-completion,zsh-completion}
)
DEFAULT_SRC_COMPILE_PARAMS=( V=1 )

BASH_COMPLETIONS=( 'completion/notmuch-completion.bash notmuch' )
ZSH_COMPLETIONS=( 'completion/notmuch-completion.zsh _notmuch' )

notmuch_src_prepare() {
    default

    # Fix install of man pages
    edo sed -e '/install /s/\.gz//' -i doc/Makefile.local

    # Fix nm invocations
    edo sed -e "/^nm /s/^nm/${CHOST}-nm/" \
        -e "/demangled.*c++filt/s/c++filt/${CHOST}-c++filt/" \
        -i lib/gen-version-script.sh

    # Fix readelf invocations
    edo sed -e "/^readelf /s/^readelf/${CHOST}-readelf/" -i test/T000-basic.sh
}

notmuch_src_compile() {
    default
    elisp_src_compile

    if option ruby; then
        edo pushd bindings/ruby

        # extconf.rb requires libnotmuch.a
        edo ruby extconf.rb

        emake
        edo popd
    fi

    if option python; then
        edo pushd bindings/python
        edo python setup.py build
        edo popd
    fi
}

notmuch_src_test() {
    # XXX: disable timeout
    edo sed -i -e 's/timeout 2m//' test/notmuch-test

    # XXX: this test always fails under sydbox
    edo rm test/T380-atomicity.sh
    edo sed -i -e '/^ *atomicity$/ d' test/notmuch-test

    esandbox disable_net
    OPTIONS= default
    esandbox enable_net
}

notmuch_src_install() {
    default

    bash-completion_src_install
    zsh-completion_src_install

    elisp_src_install

    if option ruby; then
        edo pushd bindings/ruby
        emake DESTDIR="${IMAGE}" install
        edo popd
    fi

    if option python; then
        edo pushd bindings/python
        edo python setup.py install --root="${IMAGE}"
        edo popd
    fi

    if option vim-plugin; then
        edo pushd vim
        emake prefix="${IMAGE}"/usr/share/vim/vimfiles install
        edo popd
    fi
}

